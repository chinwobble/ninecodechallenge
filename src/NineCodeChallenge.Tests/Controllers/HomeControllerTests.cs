﻿using Microsoft.AspNetCore.Mvc;
using NineCodeChallenge.Controllers;
using NineCodeChallenge.ViewModels;
using System.Collections.Generic;
using Xunit;
using System;
using System.Linq;

namespace NineCodeChallenge.Tests.Controllers
{
    public class HomeControllerTests
    {
        [Fact]
        public void Post_ReturnsBadRequest_WhenModelStateIsInvalid()
        {
            var controller = new HomeController();
            controller.ModelState.AddModelError("error", "error msg");
            var response = controller.Post(null);
            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public void Post_ReturnsBadRequestMessage_WhenModelStateIsInvalid()
        {
            var controller = new HomeController();
            controller.ModelState.AddModelError("error", "error msg");
            var response = controller.Post(null);
            Assert.Equal(@"{ Error = Could not decode request: JSON parsing failed }", ((BadRequestObjectResult)response).Value.ToString());
        }

        [Fact]
        public void Post_ReturnsCorrectViewModel()
        {
            var payload = new List<TVShow>
            {
                new TVShow
                {
                    Image = new Image { ShowImage = new Uri(@"http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg") },
                    EpisodeCount = 3,
                    Drm = true,
                    Slug = "show/16kidsandcounting",
                    Title = "16 Kids and Counting",
                }
            };
            var requestViewModel = new RequestViewModel
            {
                Payload = payload
            };
            var controller = new HomeController();
            var response = controller.Post(requestViewModel) as OkObjectResult;
            var firstResponseItem = ((ResponseViewModel)response.Value).Response.First();

            Assert.Equal(new Uri(@"http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"), firstResponseItem.Image);
            Assert.Equal("show/16kidsandcounting", firstResponseItem.Slug);
            Assert.Equal("16 Kids and Counting", firstResponseItem.Title);
        }

        [Fact]
        public void PostFilters_NoDRM_and_NoEpisodes()
        {
            var payload = new List<TVShow>
            {
                new TVShow
                {
                    EpisodeCount = 0,
                    Drm = false,
                    Title = "Should be filtered Out"
                },
                new TVShow
                {
                    Drm = true,
                    Title = "Should be filtered Out"
                },
                new TVShow
                {
                    EpisodeCount = 10,
                    Drm = true
                }
            };
            var requestViewModel = new RequestViewModel
            {
                Payload = payload
            };
            var controller = new HomeController();
            var response = controller.Post(requestViewModel) as OkObjectResult;
            var responseBody = ((ResponseViewModel)response.Value);

            Assert.Equal(1, responseBody.Response.Count);
            Assert.NotEqual("Should be filtered Out", responseBody.Response.First().Title);
        }
    }
}
