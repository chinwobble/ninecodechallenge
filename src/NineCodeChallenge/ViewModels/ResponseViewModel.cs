﻿using System;
using System.Collections.Generic;

namespace NineCodeChallenge.ViewModels
{
    public class ResponseViewModel
    {
        public ICollection<ResponseItem> Response { get; set; }
    }

    public class ResponseItem
    {
        public Uri Image { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
    }
}
