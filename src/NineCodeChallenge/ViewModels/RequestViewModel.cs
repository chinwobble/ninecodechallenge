﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NineCodeChallenge.ViewModels
{
    public class RequestViewModel
    {
        [Required]
        public List<TVShow> Payload { get; set; }
        [Required]
        public int Skip { get; set; }
        [Required]
        public int Take { get; set; }
        [Required]
        public int TotalRecords { get; set; }
    }
}
