﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NineCodeChallenge.ViewModels
{
    public class TVShow
    {
        public string Country { get; set; }
        public string Description { get; set; }
        public bool? Drm { get; set; }
        public int? EpisodeCount { get; set; }
        public string Genre { get; set; }
        public string Language { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string TvChannel { get; set; }
        public Image Image { get; set; }
    }

    public class Image
    {
        public Uri ShowImage { get; set; }
    }
}
