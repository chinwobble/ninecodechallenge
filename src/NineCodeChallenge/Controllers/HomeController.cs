﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using NineCodeChallenge.ViewModels;

namespace NineCodeChallenge.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        // GET api/values
        [HttpPost]
        public IActionResult Post([FromBody] RequestViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(new { Error = "Could not decode request: JSON parsing failed" });
            var responseList = viewModel.Payload
                .Where(x => x.EpisodeCount > 0 && x.Drm == true)
                .Select(x => new ResponseItem
                {
                    Image = x.Image?.ShowImage,
                    Slug = x.Slug,
                    Title = x.Title
                })
                .ToList();

            return Ok(new ResponseViewModel { Response = responseList });
        }
    }
}
